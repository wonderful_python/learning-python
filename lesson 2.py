# number_1 = 123124
# number_2 = 1234214.1242
# number_3 = 0b1010100
# number_4 = 0o21
# number_5 = 0x11
#
# # +
# # -
# # *
# # /
# # **
#
# print(abs(-6))  # -> 6
# print(4 & 6)  # -> 4
# print(4 | 6)  # -> 6
# print(4 ^ 6)  # -> 2
# print(4 << 6)  # -> 256
# print(4 >> 6)  # -> 0
#
#
# print(int('10001', 2)) -> 17
# print(bin(17)) -> 0b10001
# print(oct(17)) -> 0o21
# print(hex(17)) -> 0x11


# print('qwe' + ' ' + 'qwe') -> qwe qwe
# user_name = input('Your name: ')
# user_surname = input('Your surname: ')
#
# Интерполяция строк
# print('Hello ' + user_name + '  ' + user_surname) -> Выведет строку, подставив в неё данные, введённые пользователем
# print(f'Hello {user_name}  {user_surname}') аналогично предыдущему
# print('Hello {}  {}'.format(user_name, user_surname)) аналогично предыдущему

# some_string = 'qwertyuiop'
# first = some_string[3]
# print(first) -> r
#
# some_slice = some_string[0:4]
# print(some_slice) -> qwer
#
# last_el = some_slice[-1]
# print(last_el) -> r
# back_index = some_slice[4:0:-1]
# print(back_index) -> rew
#
# print(some_slice[::-1]) -> rewq
#
# my_list = ['', 'qwe', 123, 123.23, 0b101010, 123, [], [1234, '12412'], ('qwe', 123)]
# print(my_list)  # -> ['', 'qwe', 123, 123.23, 42, 123, [], [1234, '12412'], ('qwe', 123)]
# my_list.append('32qewr')
# print(my_list)  # -> ['', 'qwe', 123, 123.23, 42, 123, [], [1234, '12412'], ('qwe', 123), '32qewr']
# last_item = my_list.pop()
# print(last_item)  # -> '32qewr'
# print(my_list)  # -> ['', 'qwe', 123, 123.23, 42, 123, [], [1234, '12412'], ('qwe', 123), '32qewr']
# last_item = my_list.pop(1)
# print(last_item)  # -> 'qwe'
# print(my_list)  # ['', 123, 123.23, 42, 123, [], [1234, '12412'], ('qwe', 123)]
# my_list.remove(123)
# print(my_list)  # -> ['', 123.23, 42, 123, [], [1234, '12412'], ('qwe', 123)]
# my_list.extend(['qwe', 123, 4])
# print(my_list)  # -> ['', 123.23, 42, 123, [], [1234, '12412'], ('qwe', 123), 'qwe', 123, 4]
# my_list.extend(('qwe', 123, 4))
# print(my_list)  # ['', 123.23, 42, 123, [], [1234, '12412'], ('qwe', 123), 'qwe', 123, 4, 'qwe', 123, 4]
# el_index = my_list.index(123)
# print(el_index)  # -> 3 (index)
# print(my_list.count(123))  # -> 3 (count)
# my_list = [1, 2, 3, 4, 5]
# print(my_list * 4)  # -> [1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5]

# a = tuple(['a', 'q', 'w'])
# print(a)  # -> ('a', 'q', 'w')
# my_tuple = ('a', 'q', 'w')
# print(my_tuple)  # -> ('a', 'q', 'w')
# tpl_lst = list(my_tuple)
# print(tpl_lst)  # -> ['a', 'q', 'w']
# tpl_lst.append('w')
# print(tpl_lst)  # -> ['a', 'q', 'w', 'w']
# my_tuple = tuple(tpl_lst)
# print(my_tuple)  # -> ('a', 'q', 'w', 'w')
# print(f'тип {type(my_tuple)} объект {id(my_tuple)}')  # -> тип <class 'tuple'> объект 2219879251536
# my_tuple *= 4
# # Разные объекты!
# print(f'тип {type(my_tuple)} объект {id(my_tuple)}')  # тип <class 'tuple'> объект 2219879260928

# my_set = set()  # Если нужно пустое множество
# my = {'q', 'w', 'e', 1, 2, 1, 'q', 2, 'e'}
# print(my)  # -> {1, 2, 'q', 'e', 'w'} при каждом запуске разное
# my.add(3)
# my.add('w')
# my.add(4)
# print(my)  # -> {1, 2, 3, 4, 'q', 'e', 'w'} при каждом запуске разное
# my.remove(3)
# print(my)  # -> {'e', 1, 2, 'q', 'w', 4} при каждом запуске разное
# my.remove(1)
# print(my)  # -> {'e', 2, 'q', 'w', 4} при каждом запуске разное
# print(my.pop())  # -> e (первый элемент множества)

# my_dict = dict()
# my = {'qwe': 123, 123: 'qwe'}
# print(my.keys())  # -> ['qwe', 123]
# print(my.values())  # -> ['qwe', 123]
# print(my.items())  # -> [('qwe', 123), (123, 'qwe')]

# print(my['qwe'])  # -> 123
# print(my['qwerty'])  # -> KeyError
# print(my.get('qwe'))  # -> 123
# print(my.get('qwerty'))  # -> None

# print(bool(0))  # -> False
# print(bool(1))  # -> True
# print(bool([]))  # -> False
# print(bool(['']))  # -> True

# b = bytes('qwewqr', 'utf-8')
# b_str = b'qwrewqr'
# print(type(b))  # -> <class 'bytes'>
# print(type(b_str))  # -> <class 'bytes'>
# print(b)  # -> b'qwewqr'
# print(b_str)  # -> b'qwrewqr'

# None  # Ok google

# try:
#     first_number = int(input('first number: '))
#     second_number = int(input('second number: '))
#     try:
#         print(round(first_number / second_number, 2))
#     except ZeroDivisionError:
#         print('WTF')
# except ValueError:
#     print('number blyat`!')
# finally:
#     print('qwe')

# for
# lst_for_example = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
# for item in lst_for_example:
#     if item == 8:
#         break
#     if item % 2 == 0:
#         continue
#     print(item)

# user_number = int(input('number: '))  # 50
# number = 4 if user_number in range(0, 50) else 10
# print(number)  # -> 10

# a = (1, )
# b = (1, )
# print(id(a))  # -> id в памяти
# print(id(b))  # -> такой же 1
# print(a is b)  # -> True

