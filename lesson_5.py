# text = """4.4.4.4
# 8.8.8.8
# 192.171.34.2
# """
#
# # На запись (файл перезаписывается)
# with open('test.txt', 'w') as f:
#     f.write(text)
#
# # На чтение (ещё можно добавить 'r')
# with open('test.txt') as f:
#     print(f.readlines())
#
# # Открытие файла на запись при его отсутствии. Если есть, не будет создан
# with open('test.txt', 'x'):
#     pass
#
# # Открытие файла на дозапись
# with open('test.txt', 'a'):
#     pass
#
# # Открытие файла на чтение и запись
# with open('test.txt', '+'):
#     pass
#
# # Открытие файла в двоичном формате
# with open('test.txt', 'b'):
#     pass


# file = open('test.txt')
# print(file.closed)
# print(file.mode)
# print(file.name)
# file.close()
# print(file.closed)

# # print in file
# with open('test.txt', 'a') as f:
#     print('127.0.0.1', file=f)

# from os.path import basename, dirname, exists, isdir, isfile, join
# from os import getenv
# from os import listdir
#
# getenv('USER')
#
# print(basename(__file__))
# print(dirname(__file__))
# print(exists('test.txt'))
# print(isdir('test.txt'))
# print(isfile('test.txt'))
# path_to_file = join('home work', 'tasks_lesson_1', 'easy.py')
#
# with open(path_to_file, encoding='utf-8') as f:
#     print(f.read())
#
# print(listdir())


# from json import dump, dumps
# from json import load, loads
#
# workers = {
#     'Ivan': 0,
#     'Lyoshyad`': 1000,
#     'Petya': 10,
#     'Илья': 400,
#     'qwe': [1, 4, 5, 7]
# }


# Python -> Json
# dict == object in json
# list, tuple == array
# str == string
# int, long, float == number
# True, False == true, false
# None == null

# with open('data.json', 'w', encoding='utf-8') as f:
# #     dump(workers, f, ensure_ascii=False)
#
# with open('data.json', encoding='utf-8') as f:
#     print(type(load(f)))

# from sys import path, exit, platform
#
# print(path)
# print(platform)
# exit(-1)

import shutil
