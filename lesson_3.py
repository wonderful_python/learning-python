"""Doc for module"""


# from typing import Union
#
#
# def my_sum(x: Union[int, float] = 10, y=2) -> Union[int, float]:
# (x, y) -- обязательные, (x=1, y=2) -- необязательные
#     """
#     Doc for function
#     :param x: first number
#     :type x: int or float
#     :param y: second number
#     :return: Union[int, float]
#     """
#     res = x + y
#     return res


#
# print(my_sum(14, 4) + 101)
# print(my_sum(y=13, x=1) + 10)
# print(my_sum() + 10)
# print(my_sum.__doc__)
# my_list = [0, 1, 3, 5, 7, 0]
# new_list = list(filter(lambda x: (x % 2 == 0), my_list))
# print(my_list)


# my_list = [1, 2, 45, 664578, 1243]
# print(len(my_list))
#
# print(ord('q'))
# print(chr(113))
#
# print(abs(-23))
# print(round(1.3245, 3))
# print(divmod(21, 7))
# print(pow(2, 6))
# print(max(my_list))
# print(min(my_list))
# print(sum(my_list))

# print(list(range(7)))
# print(list(range(7, 1, -1)))
# print(list(range(-12, 7, 6)))


# Алгоритм создания функции
# Придумать информативное название
# Документация функции
# Параметры должны иметь информативные имена
# Тело функции


# list_1 = list(range(50))
# list_2 = list(range(50, 100))
# list_3 = list(zip(list_1, list_2))
# print(list_3)
# print(list(map(lambda a: a * 2, list_2)))


# def my_sum(*, x: Union[int, float], y) -> Union[int, float]:
#     # (x, y) -- обязательные, (x=1, y=2) -- необязательные
#     """
#     Doc for function
#     :param x: first number
#     :type x: int or float
#     :param y: second number
#     :return: Union[int, float]
#     """
#     res = x + y
#     return res
#
#
# print(my_sum(x=1, y=2))
