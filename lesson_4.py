# import time  # Полный импорт
# time.sleep(10)
# from time import sleep  # Частичный импорт (встроенный модуль)
# sleep(10)

# import gitlab as gl  # Сторонние модули (доустановка)

# from lesson_3 import my_sum  # Импорт из своих модулей


# new_list = [el for el in range(100) if el % 2 == 0]
# print(new_list)
# new_dict = {el: el * 2 for el in range(100) if el % 2 == 0}
# print(new_dict)
# new_set = {el**3 for el in range(5, 10)}
# print(new_set)

# import random
#
# print(random.randint(1, 100))
# print(random.random())
#
# new_list = [el for el in range(100) if el % 2 == 0]
# print(new_list)
#
# random.shuffle(new_list)
# print(new_list)
# print(random.randrange(1, 10))

#
# import math
#
# print(math.ceil(0.2))
# print(math.fabs(-0.2))
# print(math.factorial(5))
# print(math.floor(1.2))
# print(math.fmod(7, 3))
# print(math.isfinite(7.12))
# print(math.modf(7.12))
# print(math.sqrt(25))
# print(math.sin(90))
# print(math.degrees(3))
# print(math.radians(30))


# import argparse
# from typing import Union
#
#
# def my_sum(*, x: Union[int, float], y) -> Union[int, float]:
#     # (x, y) -- обязательные, (x=1, y=2) -- необязательные
#     """
#     Doc for function
#     :param x: first number
#     :type x: int or float
#     :param y: second number
#     :return: Union[int, float]
#     """
#     res = x + y
#     return res
#
#
# if __name__ == '__main__':
#     parser = argparse.ArgumentParser(
#         description="Sum of a and b",
#         formatter_class=argparse.ArgumentDefaultsHelpFormatter,
#     )
#     parser.add_argument("--a", metavar="A", type=int, help="number a")
#     parser.add_argument("--b", metavar="B", type=int, default=20, help="number b")
#     args = parser.parse_args()
#
#     print(my_sum(x=args.a, y=args.b))


# import re
#
# m = re.search(r'(\d{4})', 'asgdhgfhjfgjfg346dkfdkg')
# print(m)
# print(m.group(0))
